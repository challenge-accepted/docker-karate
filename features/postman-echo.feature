Feature: Postman Echo API GET method test

Background:
* url 'https://postman-echo.com'

Scenario: Recover the parameters sent

Given path '/get'
And request {foo":"bar1","foo2":"bar2"}
When method get
Then status 200
