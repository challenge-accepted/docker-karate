# Dockerized API Integration Tests with Karate 

This project aims to serve as a scaffold for integration test projects based on Karate framework.

### Running Integration/Acceptance Tests

The integration tests for the API is done by the provided feature files. 

> Create new feature files in `/features` folder.

To execute the tests run the following command inside project's root folder.

```
docker-compose up
```

The resulting reportes will be available at `/reports` folder.

> For the first time, it would need to create the karate container from the Dockerfile in the docker folder. So, it could take a while, dependind on the current network performance.